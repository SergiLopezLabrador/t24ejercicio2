package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.Empleados;
import com.example.demo.service.EmpleadosServicelmpl;

@RestController
@RequestMapping("/api")
public class EmpleadosController {

	@Autowired
	EmpleadosServicelmpl EmpleadosServideImpl;
	
	@GetMapping("/Empleados")
	public List<Empleados> listarEmpleados(){
		return EmpleadosServideImpl.listarEmpleados();
	}
	
	@PostMapping("/Empleados")
	public Empleados salvarEmpleado(@RequestBody Empleados Empleados) {
		
		return EmpleadosServideImpl.guardarEmpleados(Empleados);
	}
	
	@GetMapping("/Empleados/{id}")
	public Empleados EmpleadosXID(@PathVariable(name="id") Long id) {
		
		Empleados empleado_xid= new Empleados();
		empleado_xid=EmpleadosServideImpl.empleadosXID(id);
		System.out.println("Empleado XID: "+empleado_xid);
		
		return empleado_xid;
	}
	
	@PutMapping("/Empleados/{id}")
	public Empleados actualizarEmpleado(@PathVariable(name="id")Long id,@RequestBody Empleados Empleados) {
		
		Empleados Empleados_seleccionado= new Empleados();
		Empleados Empleados_actualizado= new Empleados();
		
		Empleados_seleccionado= EmpleadosServideImpl.empleadosXID(id);
		
		Empleados_seleccionado.setNombre(Empleados.getNombre());
		Empleados_seleccionado.setApellidos(Empleados.getApellidos());
		Empleados_seleccionado.setDepartamento(Empleados.getDepartamento());

		
		Empleados_actualizado = EmpleadosServideImpl.actualizarEmpleados(Empleados_seleccionado);
		
		System.out.println("El empleado actualizado es: "+ Empleados_actualizado);
		
		return Empleados_actualizado;
	}
	
	@DeleteMapping("/Empleados/{id}")
	public void eleiminarEmpleados(@PathVariable(name="id")Long id) {
		EmpleadosServideImpl.eliminarEmpleados(id);
	}
	
}
