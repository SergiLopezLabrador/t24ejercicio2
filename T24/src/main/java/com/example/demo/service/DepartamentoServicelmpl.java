package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.DepartamentoDAO;
import com.example.demo.dto.Departamento;

@Service
public class DepartamentoServicelmpl implements DepartamentoService{

	@Autowired
	DepartamentoDAO departamentodao;
	
	@Override
	public List<Departamento> listarDepartamento() {
		
		return departamentodao.findAll();
	}
	
	@Override
	public Departamento guardarDepartamento(Departamento departamentos) {
		
		return departamentodao.save(departamentos);
	}

	@Override
	public Departamento departamentoXID(Long id) {
		
		return departamentodao.findById(id).get();
	}

	@Override
	public Departamento actualizarDepartamento(Departamento departamentos) {
		
		return departamentodao.save(departamentos);
	}

	@Override
	public void eliminarDepartamento(Long id) {
		
		departamentodao.deleteById(id);
		
	}
	
}
