package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.Departamento;

public interface DepartamentoService {

	public List<Departamento> listarDepartamento(); 
	
	public Departamento guardarDepartamento(Departamento departamento);	
	
	public Departamento departamentoXID(Long id); 
	
	public Departamento actualizarDepartamento(Departamento departamento); 
	
	public void eliminarDepartamento(Long id);
	
}
