package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.EmpleadosDAO;
import com.example.demo.dto.Empleados;

@Service
public class EmpleadosServicelmpl implements EmpleadosService{
	
	//Utilizamos los metodos de la interface IClienteDAO, es como si instaciaramos.
		@Autowired
		EmpleadosDAO empleadosdao;
		
		@Override
		public List<Empleados> listarEmpleados() {
			
			return empleadosdao.findAll();
		}

		@Override
		public Empleados guardarEmpleados(Empleados empleados) {
			
			return empleadosdao.save(empleados);
		}

		@Override
		public Empleados empleadosXID(Long id) {
			
			return empleadosdao.findById(id).get();
		}

		@Override
		public Empleados actualizarEmpleados(Empleados empleados) {
			
			return empleadosdao.save(empleados);
		}

		@Override
		public void eliminarEmpleados(Long id) {
			
			empleadosdao.deleteById(id);
			
		}

}
