package com.example.demo.service;

import java.util.List;
import com.example.demo.dto.Empleados;

public interface EmpleadosService {

		public List<Empleados> listarEmpleados();
		
		public Empleados guardarEmpleados(Empleados empleados);	
		
		public Empleados empleadosXID(Long id);
		
		public Empleados actualizarEmpleados(Empleados empleados); 
		
		public void eliminarEmpleados(Long id);
}
