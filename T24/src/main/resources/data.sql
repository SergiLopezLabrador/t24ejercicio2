DROP table IF EXISTS empleados;
DROP table IF EXISTS departamento;

create table departamento(
    id Long not null auto_increment primary key,
    nombre varchar(250) null,
    presupuesto int
);
create table empleados(
    id Long not null auto_increment primary key,
    nombre varchar(250) null,
    apellidos varchar(250) null,
    departamento int,
    FOREIGN KEY (departamento) REFERENCES departamento(id)
);

insert into departamento (nombre, presupuesto)values('sergi', 50);
insert into empleados (nombre, apellidos,departamento)values('manolo', 'jimenez', 1);
